package com.digicore.assessment.services;

import com.digicore.assessment.RequestBodies.CreateUserRequest;
import com.digicore.assessment.RequestBodies.DepositRequest;
import com.digicore.assessment.RequestBodies.LoginUserRequest;
import com.digicore.assessment.RequestBodies.WithdrawRequest;
import com.digicore.assessment.ResponseBodies.*;
import com.digicore.assessment.config.UserDetailsImplService;
import com.digicore.assessment.entity.Account;
import com.digicore.assessment.entity.Transaction;
import com.digicore.assessment.entity.UserEntity;
import com.digicore.assessment.enums.TransactionType;
import com.digicore.assessment.generator.AccountNumberGenerator;
import com.digicore.assessment.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class UserService {

    private HashMap<String, UserEntity> acctNoToUserStore;

    private HashMap<String, String> acctNoToAcctNameStore;

    private HashMap<String, UserEntity> userAccountNameToUserStore;

    private AuthenticationManager authenticationManager;

    private UserDetailsImplService userDetailsService;

    private JwtTokenProvider jwtTokenProvider;

    private PasswordEncoder passwordEncoder;

    private HashMap<String, List<Transaction>> transactionDb;

    @Autowired
    public UserService(HashMap<String, UserEntity> acctNoToUserStore, HashMap<String, String> acctNoToAcctNameStore, HashMap<String, UserEntity> userAccountNameToUserStore, AuthenticationManager authenticationManager, UserDetailsImplService userDetailsService, JwtTokenProvider jwtTokenProvider, PasswordEncoder passwordEncoder, HashMap<String, List<Transaction>> transactionDb) {
        this.acctNoToUserStore = acctNoToUserStore;
        this.acctNoToAcctNameStore = acctNoToAcctNameStore;
        this.userAccountNameToUserStore = userAccountNameToUserStore;
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.jwtTokenProvider = jwtTokenProvider;
        this.passwordEncoder = passwordEncoder;
        this.transactionDb = transactionDb;
    }


    public ResponseEntity<CreatedUserResponse> createUser(CreateUserRequest userRequest){
        if(userRequest.getInitialDeposit() < 500){

            CreatedUserResponse userResponse = new CreatedUserResponse(false,
                    "Initial deposit of "+userRequest.getInitialDeposit()
                            +" was rejected, Initial deposit minimum is #500");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(userResponse);

        }else if(acctNoToAcctNameStore.containsValue(userRequest.getAccountName())){

            CreatedUserResponse userResponse = new CreatedUserResponse(false,
                    "User Account was not created as a User with account name: " +userRequest.getAccountName()
                            +" already exist");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(userResponse);

        }else {

            String acctNo = AccountNumberGenerator.generate();
            while (acctNoToUserStore.containsKey(acctNo)) {
                acctNo = AccountNumberGenerator.generate();
            }

            Account account = new Account(acctNo,userRequest.getAccountName(), userRequest.getInitialDeposit());
            UserEntity user = new UserEntity(userRequest.getAccountName(),
                    passwordEncoder.encode(userRequest.getAccountPassword()), account);

            user.setAccount(account);
            Transaction transaction = new Transaction(LocalDateTime.now(),TransactionType.DEPOSIT,
                    userRequest.getInitialDeposit(),user.getAccount().getAccountBalance());
            List<Transaction> userTransaction;
            if (transactionDb.containsKey(acctNo)) {
                userTransaction = transactionDb.get(acctNo);
            }else {
                userTransaction = new ArrayList<>();

            }
            userTransaction.add(transaction);

            acctNoToUserStore.put(acctNo, user);
            acctNoToAcctNameStore.put(acctNo, user.getAccountName());
            transactionDb.put(acctNo, userTransaction);
            userAccountNameToUserStore.put(userRequest.getAccountName(), user);

            CreatedUserResponse userResponse = new CreatedUserResponse(true,
                    "User Account successfully created", acctNo);
            return ResponseEntity.status(HttpStatus.OK).body(userResponse);
        }
    }

    public ResponseEntity<String> login(LoginUserRequest userRequest) throws Exception {
        String acctNo = userRequest.getAccountNumber();
        String username = acctNoToAcctNameStore.get(acctNo);
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(username,
                            userRequest.getAccountPassword())
            );
        }
        catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }

        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(username);

        final String jwt = jwtTokenProvider.generateToken(userDetails);

        return ResponseEntity.ok(jwt);
    }

    public ResponseEntity<AccountInfoResponse> getAccountInfo(String acctNo){
        if(!acctNoToUserStore.containsKey(acctNo)){
            AccountInfoResponse response = new AccountInfoResponse(false,"Account does not exist");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        UserEntity userEntity = acctNoToUserStore.get(acctNo);
        AccountInfoResponse response = new AccountInfoResponse(true,"Account statement is as below:",
                userEntity.getAccount());
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    public ResponseEntity<?> getAccountTransactionInfo(String acctNo){
        if(!transactionDb.containsKey(acctNo)){

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Account does not exist");
        }
        List<Transaction> transactionList = transactionDb.get(acctNo);
        List<TransactionInfoResponse> transactionInfoList = new ArrayList<>();
        for (Transaction transaction : transactionList) {
            TransactionInfoResponse transactionInfoResponse = new TransactionInfoResponse(
                    transaction.getTransactionDate(), "" + transaction.getTransactionType().toString(), "Successful",
                    transaction.getTransactionAmount(), transaction.getAccountBalance()
            );
            transactionInfoList.add(transactionInfoResponse);
        }

        return ResponseEntity.status(HttpStatus.OK).body(transactionInfoList);
    }

    public ResponseEntity<DepositResponse> doDeposit(DepositRequest depositRequest){
        String acctNo = depositRequest.getAccountNumber();
        UserEntity user = acctNoToUserStore.get(depositRequest.getAccountNumber());
        if(!acctNoToUserStore.containsKey(acctNo)){
            DepositResponse depositResponse = new DepositResponse(false, "Account does not exist");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(depositResponse);
        }
        if(depositRequest.getAmount() > 1000000 || depositRequest.getAmount() < 1){
            DepositResponse depositResponse = new DepositResponse(false, "Deposit exceeds or is lower than limit; " +
                    "Maximum deposit is #1,000,000 and minimum is #1");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(depositResponse);
        }
        user.getAccount().setAccountBalance(user.getAccount().getAccountBalance() + depositRequest.getAmount());
        acctNoToUserStore.put(acctNo, user);
        userAccountNameToUserStore.put(user.getAccountName(), user);

        Transaction transaction = new Transaction(LocalDateTime.now(),TransactionType.DEPOSIT,
                depositRequest.getAmount(),user.getAccount().getAccountBalance());

        List<Transaction> userTransaction = transactionDb.get(acctNo);
        userTransaction.add(transaction);
        transactionDb.put(acctNo, userTransaction);

        DepositResponse depositResponse = new DepositResponse(true, "Deposit of #" +depositRequest.getAmount()
        +" was successful");
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(depositResponse);
    }

    public ResponseEntity<WithdrawResponse> doWithdrawal(WithdrawRequest withdrawRequest){
        String acctNo = withdrawRequest.getAccountNumber();
        UserEntity user = acctNoToUserStore.get(withdrawRequest.getAccountNumber());
        if(!acctNoToUserStore.containsKey(acctNo)){
            WithdrawResponse response = new WithdrawResponse(false,"Account does not exist");
            return ResponseEntity.status(HttpStatus.OK).body(response);
        }
        if(withdrawRequest.getWithdrawnAmount() < 1){
            WithdrawResponse response = new WithdrawResponse(false,"Withdrawal of "+withdrawRequest.getWithdrawnAmount()
                    +" is below withdraw limit");
            return ResponseEntity.status(HttpStatus.OK).body(response);
        }
        if(user.getAccount().getAccountBalance() - withdrawRequest.getWithdrawnAmount() < 500){
            WithdrawResponse response = new WithdrawResponse(false,"Insufficient balance, as you must have a " +
                    "remainder balance of #500");
            return ResponseEntity.status(HttpStatus.OK).body(response);
        }

        user.getAccount().setAccountBalance(user.getAccount().getAccountBalance() - withdrawRequest.getWithdrawnAmount());
        acctNoToUserStore.put(acctNo, user);
        userAccountNameToUserStore.put(user.getAccountName(), user);

        Transaction transaction = new Transaction(LocalDateTime.now(),TransactionType.WITHDRAWAL,
                withdrawRequest.getWithdrawnAmount(),user.getAccount().getAccountBalance());

        List<Transaction> userTransaction = transactionDb.get(acctNo);
        userTransaction.add(transaction);
        transactionDb.put(acctNo, userTransaction);
        WithdrawResponse response = new WithdrawResponse(true,"Withdrawal of " +withdrawRequest.getWithdrawnAmount()
        +" was successful; available balance is #" +user.getAccount().getAccountBalance());
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
