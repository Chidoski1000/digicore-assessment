package com.digicore.assessment.controllers;

import com.digicore.assessment.RequestBodies.CreateUserRequest;
import com.digicore.assessment.RequestBodies.DepositRequest;
import com.digicore.assessment.RequestBodies.LoginUserRequest;
import com.digicore.assessment.RequestBodies.WithdrawRequest;
import com.digicore.assessment.ResponseBodies.*;
import com.digicore.assessment.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AccountController {

    @Autowired
    UserService userService;


    @GetMapping("/account_info/{accountNo}")
    public ResponseEntity<AccountInfoResponse> getAccountInfo(@PathVariable ("accountNo") String acctNo){
        return userService.getAccountInfo(acctNo);
    }

    @GetMapping("/account_statement/{accountNo}")
    public ResponseEntity<?> getAccountTransactionInfo(@PathVariable ("accountNo") String acctNo){
        return userService.getAccountTransactionInfo(acctNo);
    }

    @PostMapping("/deposit")
    public ResponseEntity<DepositResponse> deposit(@RequestBody DepositRequest depositRequest){
        return userService.doDeposit(depositRequest);
    }

    @PostMapping("/withdrawal")
    public ResponseEntity<WithdrawResponse> withdraw(@RequestBody WithdrawRequest withdrawRequest){
        return userService.doWithdrawal(withdrawRequest);
    }

    @PostMapping("/create_account")
    public ResponseEntity<CreatedUserResponse> createAccount(@RequestBody CreateUserRequest userRequest){
       return userService.createUser(userRequest);
    }


    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody LoginUserRequest userRequest) throws Exception {
        return userService.login(userRequest);
    }
}
