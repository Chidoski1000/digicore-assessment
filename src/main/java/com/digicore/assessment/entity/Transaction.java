package com.digicore.assessment.entity;

import com.digicore.assessment.enums.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Transaction {
    private LocalDateTime transactionDate;
    private TransactionType transactionType;
    private Double transactionAmount;
    private Double accountBalance;
}
