package com.digicore.assessment.generator;

import java.util.Random;

public class AccountNumberGenerator {

    public static String generate() {
        String[] numbers = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < 10; i++){
            int index = random.nextInt(numbers.length);
            sb.append(numbers[index]);
        }

        return sb.toString();
    }
}
