package com.digicore.assessment.ResponseBodies;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TransactionInfoResponse {

    private LocalDateTime transactionDate;
    private String transactionType;
    private String narration;
    private Double amount;
    private Double accountBalance;
}
