package com.digicore.assessment.ResponseBodies;

import com.digicore.assessment.entity.Account;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AccountInfoResponse {
    private boolean success;
    private String message;
    private Account account;

    public AccountInfoResponse(boolean success, String message) {
        this.success = success;
        this.message = message;
    }
}
