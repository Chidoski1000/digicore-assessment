package com.digicore.assessment.ResponseBodies;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CreatedUserResponse {
    private boolean status;
    private String message;
    private String accountNumber;
//    private HttpStatus status;


    public CreatedUserResponse(boolean status, String message) {
        this.status = status;
        this.message = message;
    }
}
