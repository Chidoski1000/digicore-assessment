package com.digicore.assessment.config;

import com.digicore.assessment.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
public class UserDetailsImplService implements UserDetailsService {


    @Autowired
    private HashMap<String, UserEntity> userAccountNameToUserStore;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        return UserDetailsImpl.buildUserDetail(userAccountNameToUserStore.get(s));
    }

}
