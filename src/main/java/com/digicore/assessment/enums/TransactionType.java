package com.digicore.assessment.enums;

public enum TransactionType {
    DEPOSIT, WITHDRAWAL;
}
