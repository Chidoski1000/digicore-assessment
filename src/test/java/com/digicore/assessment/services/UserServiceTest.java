package com.digicore.assessment.services;

import com.digicore.assessment.RequestBodies.CreateUserRequest;
import com.digicore.assessment.entity.UserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userServices;

    @Autowired
    private HashMap<String, UserEntity> datastore;

    @Test
    void createUser() {
        CreateUserRequest request = new CreateUserRequest("Test",
                "password", 5000d);
        userServices.createUser(request);
        assertEquals(datastore.size(), 1);
    }


}