package com.digicore.assessment;

public class PerfectSquareCount {

    public static void main(String[] args) {
        System.out.println(perfectSquareCount(4,5));
        System.out.println(perfectSquareCount(5,5));
        System.out.println(perfectSquareCount(5,4));
        System.out.println(perfectSquareCount(7,3));
    }

    public static int perfectSquareCount(int x, int y){
        int num = 0;
        if(x == y){
            for(int i = x; i > 0; i--){
                num += Math.pow(i,2);
            }
            return num;
        }else {
            int max;
            int min;
            if (x > y) {
                max = x;
                min = y;
            } else {
                min = x;
                max = y;
            }

            while(min > 0){
               num += (max * min);
               max--;
               min--;
            }
            return num;
        }

    }
}
